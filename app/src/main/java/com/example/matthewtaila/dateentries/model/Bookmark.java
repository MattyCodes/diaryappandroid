package com.example.matthewtaila.dateentries.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by matthewtaila on 8/4/16.
 */
public class Bookmark implements Parcelable {

    public static final boolean BOOKMARK_FALSE = false;
    public static final boolean BOOKMARK_TRUE = true;


    boolean bookmarked = false;
    public Bookmark() {
    }

    protected Bookmark(Parcel in) {
        bookmarked = in.readByte() != 0;
    }

    public static final Creator<Bookmark> CREATOR = new Creator<Bookmark>() {
        @Override
        public Bookmark createFromParcel(Parcel in) {
            return new Bookmark(in);
        }

        @Override
        public Bookmark[] newArray(int size) {
            return new Bookmark[size];
        }
    };

    public boolean isBookmarked() {
        return bookmarked;
    }
    public void setBookmark(boolean bookmarked) {
        this.bookmarked = bookmarked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (bookmarked ? 1 : 0));
    }
}
