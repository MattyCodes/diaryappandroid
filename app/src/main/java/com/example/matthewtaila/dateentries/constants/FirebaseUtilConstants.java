package com.example.matthewtaila.dateentries.constants;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by matthewtaila on 7/28/16.
 */
public class FirebaseUtilConstants {

    public static final DatabaseReference DATE_ENTRIES_REF = FirebaseDatabase.getInstance().getReference();
    public static final String FB_USERS = "Users List";
    public static final String FB_BOOKMARKED = "Bookmarked";
    public static final String FB_MOOD = "Mood";
    public static final String FB_WEATHER = "Weather";


}
