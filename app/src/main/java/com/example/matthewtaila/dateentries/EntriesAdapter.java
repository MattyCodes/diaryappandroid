package com.example.matthewtaila.dateentries;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.matthewtaila.dateentries.model.Entries;

import java.util.ArrayList;

/**
 * Created by matthewtaila on 9/8/16.
 */
public class EntriesAdapter extends RecyclerView.Adapter<EntriesAdapter.ViewHolder> {

    ArrayList<Entries> dateEntriesList;
    Context context;

    public EntriesAdapter(ArrayList<Entries> dateEntriesList, Context context) {
        this.dateEntriesList = dateEntriesList;
        this.context = context;
    }

    private Context getContext(){
        return context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View dateView = inflater.inflate(R.layout.item_card_view_date_entry, parent, false);
        ViewHolder viewHolder = new ViewHolder(dateView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Entries entry = dateEntriesList.get(position);
        holder.tvTitle.setText(entry.getTitle());

    }

    @Override
    public int getItemCount() {
        return dateEntriesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvDescription;
        View containter;

        public ViewHolder(View itemView){
            super(itemView);
            tvTitle = (TextView)itemView.findViewById(R.id.tv_title);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
            containter = (View)itemView.findViewById(R.id.cv_container);
        }
    }
}
