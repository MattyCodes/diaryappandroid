package com.example.matthewtaila.dateentries.constants;

/**
 * Created by matthewtaila on 7/28/16.
 */
public class Constants {

    public static final String KEY_USER = "user";
    public static final String KEY_WEATHER = "weather";
    public static final String KEY_MOOD = "mood";
    // - MOOD
    public static final String MOOD_TODAY = "How are you feeling today?";
    public static final String MOOD_EMPTY = "How are you feeling today?";
    public static final String MOOD_HAPPY = "Happy";
    public static final String MOOD_NEUTRAL = "Neutral";
    public static final String MOOD_VERYHAPPY = "Very Happy";
    public static final String MOOD_COOL = "Cool";
    public static final String MOOD_SAD = "Sad";
    public static final String MOOD_SHITTY = "Shitty";
    public static final String MOOD_CHEEKY = "Cheeky";
    public static final String MOOD_SILLY = "Silly";
    // - WEATHER
    public static final String WEATHER_EMPTY = "How's the weather today?";
    public static final String WEATHER_SUNNY = "Sunny";
    public static final String WEATHER_CLOUDY = "Cloudy";
    public static final String WEATHER_WINDY = "Windy";
    public static final String WEATHER_DRIZZLE = "Drizzling";
    public static final String WEATHER_RAINING = "Raining";
    public static final String WEATHER_THUNDER = "Thunderstorm";
    public static final String WEATHER_SNOW = "Snowing";
    public static final String WEATHER_FOG = "Foggy";



}
