package com.example.matthewtaila.dateentries;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.matthewtaila.dateentries.constants.Constants;
import com.example.matthewtaila.dateentries.constants.FirebaseUtilConstants;
import com.example.matthewtaila.dateentries.model.Bookmark;
import com.example.matthewtaila.dateentries.model.Entries;
import com.example.matthewtaila.dateentries.model.Mood;
import com.example.matthewtaila.dateentries.model.User;
import com.example.matthewtaila.dateentries.model.Weather;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class DateEntryActivity extends AppCompatActivity implements View.OnClickListener{


    // - UI
    TextView tv_dayNum;
    TextView tv_month;
    TextView tv_year;
    ImageButton moodIcon;
    ImageButton weatherIcon;
    ImageButton bookmarkIcon;
    RecyclerView recyclerView;

    // Logic
    ArrayList<Entries> entriesArrayList;
    User user;
    Bookmark bookmark;
    Mood mood;
    Weather weather;

    // FIrebase
    DatabaseReference moodRef;
    DatabaseReference weatherRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_entry);
        user = getIntent().getParcelableExtra(Constants.KEY_USER);

        referenceViews();
        createEntriesList();
        setupRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupFirebaseReferences();
        setDateDisplay();
        stringDate();
        moodFirebasePull();
        weatherFirebasePull();
    }

    private void createEntriesList() {
        String[] entryTitlesArray = {
                "Did I exercise today?",
                "Did I achieve my goals today?",
                "What did I buy today?",
                "How did I help my friends today?",
                "Did I help my family today?",
                "What was productive about my day?",
                "How can I make tomorrow better?",
                "What can I be grateful about today?",
                "What challenges did I have today?",
                "Did I meet someone new today? No"
        };

        entriesArrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Entries entry = new Entries();
            entry.setTitle(entryTitlesArray[i]);
            entriesArrayList.add(entry);
        }
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2, 1, false));
        EntriesAdapter entriesAdapter = new EntriesAdapter(entriesArrayList, this);
        recyclerView.setAdapter(entriesAdapter);
    }

    private void weatherFirebasePull() {
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                weather = dataSnapshot.getValue(Weather.class);
                if (weather == null){
                    weather = new Weather();
                    weather.setWeather(Constants.WEATHER_EMPTY);
                } else {
                    setWeatherIcon();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        weatherRef.addListenerForSingleValueEvent(valueEventListener);
    }

    public void setWeatherIcon(){
        weatherIcon.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        switch (mood.getMood()) {
            case Constants.WEATHER_CLOUDY:
                weatherIcon.setImageResource(R.drawable.weather_cloudy);
                break;
            case Constants.WEATHER_SUNNY:
                weatherIcon.setImageResource(R.drawable.weather_sunny);
                break;
            case Constants.WEATHER_THUNDER:
                weatherIcon.setImageResource(R.drawable.weather_thunder);
                break;
            case Constants.WEATHER_DRIZZLE:
                weatherIcon.setImageResource(R.drawable.weather_drizzle);
                break;
            case Constants.WEATHER_FOG:
                weatherIcon.setImageResource(R.drawable.weather_fog);
                break;
            case Constants.WEATHER_RAINING:
                weatherIcon.setImageResource(R.drawable.weather_raining);
                break;
            case Constants.WEATHER_WINDY:
                weatherIcon.setImageResource(R.drawable.weather_windy);
                break;
            case Constants.WEATHER_SNOW:
                weatherIcon.setImageResource(R.drawable.weather_snowy);
                break;
            default:
                weatherIcon.setImageResource(R.drawable.standard_weather);
                weatherIcon.setColorFilter(ContextCompat.getColor(this, android.R.color.black));
                break;
        }


        if (weather.getWeather().equals(Constants.WEATHER_EMPTY)){
            weatherIcon.setImageResource(R.drawable.standard_weather);
        } else if(weather.getWeather().equals(Constants.WEATHER_SUNNY)){
            weatherIcon.setImageResource(R.drawable.weather_sunny);
            weatherIcon.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if(weather.getWeather().equals(Constants.WEATHER_RAINING)){
            weatherIcon.setImageResource(R.drawable.weather_raining);
            weatherIcon.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if(weather.getWeather().equals(Constants.WEATHER_CLOUDY)){
            weatherIcon.setImageResource(R.drawable.weather_cloudy);
            weatherIcon.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if(weather.getWeather().equals(Constants.WEATHER_DRIZZLE)){
            weatherIcon.setImageResource(R.drawable.weather_drizzle);
            weatherIcon.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if(weather.getWeather().equals(Constants.WEATHER_SNOW)){
            weatherIcon.setImageResource(R.drawable.weather_snowy);
            weatherIcon.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if(weather.getWeather().equals(Constants.WEATHER_WINDY)){
            weatherIcon.setImageResource(R.drawable.weather_windy);
            weatherIcon.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if(weather.getWeather().equals(Constants.WEATHER_THUNDER)){
            weatherIcon.setImageResource(R.drawable.weather_thunder);
            weatherIcon.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if(weather.getWeather().equals(Constants.WEATHER_FOG)){
            weatherIcon.setImageResource(R.drawable.weather_fog);
            weatherIcon.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        }
    }

    private void setupFirebaseReferences() {
        moodRef = FirebaseUtilConstants.DATE_ENTRIES_REF.child(FirebaseUtilConstants.FB_MOOD).child(user.getKey()).child(DateEntryActivity.stringDate());
        weatherRef = FirebaseUtilConstants.DATE_ENTRIES_REF.child(FirebaseUtilConstants.FB_WEATHER).child(user.getKey()).child(DateEntryActivity.stringDate());
    }

    private void moodFirebasePull() {
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mood = dataSnapshot.getValue(Mood.class);
                if (mood == null){
                    mood = new Mood();
                    mood.setMood(Constants.MOOD_EMPTY);
                } else {
                    setMoodIcon();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        moodRef.addListenerForSingleValueEvent(valueEventListener);
    }

    public void setMoodIcon(){
        moodIcon.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        switch (mood.getMood()) {
            case Constants.MOOD_HAPPY:
                moodIcon.setImageResource(R.drawable.mood_happy);
                break;
            case Constants.MOOD_VERYHAPPY:
                moodIcon.setImageResource(R.drawable.mood_veryhappy);
                break;
            case Constants.MOOD_SAD:
                moodIcon.setImageResource(R.drawable.mood_sad);
                break;
            case Constants.MOOD_COOL:
                moodIcon.setImageResource(R.drawable.mood_cool);
                break;
            case Constants.MOOD_CHEEKY:
                moodIcon.setImageResource(R.drawable.mood_cheeky);
                break;
            case Constants.MOOD_SILLY:
                moodIcon.setImageResource(R.drawable.mood_silly);
                break;
            case Constants.MOOD_SHITTY:
                moodIcon.setImageResource(R.drawable.mood_shitty);
                break;
            case Constants.MOOD_NEUTRAL:
                moodIcon.setImageResource(R.drawable.mood_neutral);
                break;
            default:
                moodIcon.setImageResource(R.drawable.standard_mood);
                moodIcon.setColorFilter(ContextCompat.getColor(this, android.R.color.black));
                break;
        }
    }

    private void referenceViews() {

        // - reference
        tv_dayNum = (TextView)findViewById(R.id.tv_dayNumber);
        tv_month = (TextView)findViewById(R.id.tv_month);
        tv_year = (TextView)findViewById(R.id.tv_year);
        moodIcon = (ImageButton) findViewById(R.id.ibtn_mood);
        weatherIcon = (ImageButton)findViewById(R.id.ibtn_weather);
        bookmarkIcon = (ImageButton)findViewById(R.id.ibtn_bookmark);
        recyclerView = (RecyclerView)findViewById(R.id.rv_entries);

        // - onClick
        moodIcon.setOnClickListener(this);
        weatherIcon.setOnClickListener(this);
        bookmarkIcon.setOnClickListener(this);
    }

    private void setDateDisplay() {
        Date today = java.util.Calendar.getInstance().getTime();
        SimpleDateFormat dayNum = new SimpleDateFormat("dd");
        SimpleDateFormat month = new SimpleDateFormat("MM");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");
        tv_dayNum.setText(dayNum.format(today));
        tv_month.setText(month.format(today));
        tv_year.setText(year.format(today));
    }

    public static String stringDate() {
        Date today = java.util.Calendar.getInstance().getTime();
        SimpleDateFormat x = new SimpleDateFormat("EEE, MM d, ''yyyy", Locale.US);
        return  x.format(today);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ibtn_bookmark:
                startEditFieldActivity();
                break;
            case R.id.ibtn_mood:
                startEditFieldActivity();
                break;
            case R.id.ibtn_weather:
                startEditFieldActivity();
                break;
        }
    }

    private void startEditFieldActivity() {
        Intent intent = new Intent(this, EditFieldActivity.class);
        intent.putExtra(Constants.KEY_USER, user);
        intent.putExtra(Constants.KEY_MOOD, mood);
        intent.putExtra(Constants.KEY_WEATHER, weather);
        startActivity(intent);
    }
}
