package com.example.matthewtaila.dateentries.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.matthewtaila.dateentries.constants.Constants;

/**
 * Created by matthewtaila on 9/1/16.
 */
public class Weather implements Parcelable {

    String weather = Constants.WEATHER_EMPTY;

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public Weather() {
    }

    protected Weather(Parcel in) {
        weather = in.readString();
    }

    public static final Creator<Weather> CREATOR = new Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(weather);
    }
}
