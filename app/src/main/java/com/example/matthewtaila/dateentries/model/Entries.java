package com.example.matthewtaila.dateentries.model;

/**
 * Created by matthewtaila on 9/8/16.
 */
public class Entries {

    String title;
    String description;

    public Entries() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
