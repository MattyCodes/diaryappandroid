package com.example.matthewtaila.dateentries;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.matthewtaila.dateentries.constants.Constants;
import com.example.matthewtaila.dateentries.constants.FirebaseUtilConstants;
import com.example.matthewtaila.dateentries.model.Bookmark;
import com.example.matthewtaila.dateentries.model.Mood;
import com.example.matthewtaila.dateentries.model.User;
import com.example.matthewtaila.dateentries.model.Weather;
import com.google.firebase.database.DatabaseReference;

public class EditFieldActivity extends AppCompatActivity implements View.OnClickListener {

    // - UI
    ImageButton setBookmark;
    ImageButton moodHappy;
    ImageButton moodNeutral;
    ImageButton moodVeryHappy;
    ImageButton moodCool;
    ImageButton moodShitty;
    ImageButton moodSad;
    ImageButton moodHorny;
    ImageButton moodSilly;
    Button btnDone;
    TextView tvSetMood;
    ImageButton weatherSunny;
    ImageButton weatherCloudy;
    ImageButton weatherWindy;
    ImageButton weatherDrizzle;
    ImageButton weatherRaining;
    ImageButton weatherThunder;
    ImageButton weatherFoggy;
    ImageButton weatherSnowing;
    TextView tv_weather;

    // - Firebase
    DatabaseReference bookmarkRef;
    DatabaseReference moodRef;
    DatabaseReference weatherRef;
    // - logic
    Bookmark bookmark = new Bookmark();
    Mood moodToday;
    Weather weatherToday;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_field);
        user = getIntent().getParcelableExtra(Constants.KEY_USER);
        moodToday = getIntent().getParcelableExtra(Constants.KEY_MOOD);
        weatherToday = getIntent().getParcelableExtra(Constants.KEY_WEATHER);
        setupFirebaseRef();
        referenceView();
        checkMood(moodToday);
        checkWeather(weatherToday);
    }

    private void setupFirebaseRef() {
        bookmarkRef = FirebaseUtilConstants.DATE_ENTRIES_REF.child(FirebaseUtilConstants.FB_BOOKMARKED).child(user.getKey()).child(DateEntryActivity.stringDate());
        moodRef = FirebaseUtilConstants.DATE_ENTRIES_REF.child(FirebaseUtilConstants.FB_MOOD).child(user.getKey()).child(DateEntryActivity.stringDate());
        weatherRef = FirebaseUtilConstants.DATE_ENTRIES_REF.child(FirebaseUtilConstants.FB_WEATHER).child(user.getKey()).child(DateEntryActivity.stringDate());
    }

    private void referenceView() {
        btnDone = (Button) findViewById(R.id.btn_done);
        // MOOD
        tvSetMood = (TextView) findViewById(R.id.tv_mood);
        setBookmark = (ImageButton) findViewById(R.id.ibtn_setBookmark);
        moodHappy = (ImageButton) findViewById(R.id.ibtn_happy);
        moodVeryHappy = (ImageButton) findViewById(R.id.ibtn_veryHappy);
        moodNeutral = (ImageButton) findViewById(R.id.ibtn_neutral);
        moodCool = (ImageButton) findViewById(R.id.ibtn_cool);
        moodShitty = (ImageButton) findViewById(R.id.ibtn_shitty);
        moodSad = (ImageButton) findViewById(R.id.ibtn_sad);
        moodHorny = (ImageButton) findViewById(R.id.ibtn_horny);
        moodSilly = (ImageButton) findViewById(R.id.ibtn_silly);
        // WEATHER;
        tv_weather = (TextView)findViewById(R.id.tv_weather);
        weatherCloudy = (ImageButton)findViewById(R.id.ibtn_cloudy);
        weatherSunny = (ImageButton)findViewById(R.id.ibtn_sunny);
        weatherDrizzle = (ImageButton)findViewById(R.id.ibtn_drizzle);
        weatherRaining = (ImageButton)findViewById(R.id.ibtn_raining);
        weatherFoggy = (ImageButton)findViewById(R.id.ibtn_foggy);
        weatherSnowing = (ImageButton)findViewById(R.id.ibtn_snowing);
        weatherThunder = (ImageButton)findViewById(R.id.ibtn_thunderstorm);
        weatherWindy = (ImageButton)findViewById(R.id.ibtn_windy);
        // - onClick
        setBookmark.setOnClickListener(this);
        moodHappy.setOnClickListener(this);
        moodVeryHappy.setOnClickListener(this);
        moodNeutral.setOnClickListener(this);
        moodCool.setOnClickListener(this);
        moodShitty.setOnClickListener(this);
        moodHorny.setOnClickListener(this);
        moodSad.setOnClickListener(this);
        moodSilly.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        weatherWindy.setOnClickListener(this);
        weatherSnowing.setOnClickListener(this);
        weatherFoggy.setOnClickListener(this);
        weatherRaining.setOnClickListener(this);
        weatherSunny.setOnClickListener(this);
        weatherThunder.setOnClickListener(this);
        weatherDrizzle.setOnClickListener(this);
        weatherCloudy.setOnClickListener(this);
    }

    public void setBookmarked(boolean bookmarked) {
        bookmark.setBookmark(bookmarked);
        bookmarkRef.setValue(bookmark);
        checkBookmark(bookmark);
    }

    private void checkBookmark(Bookmark bookmark) {
        if (bookmark.isBookmarked() == Bookmark.BOOKMARK_FALSE) {
            setBookmark.setColorFilter(ContextCompat.getColor(this, R.color.black));
            setBookmark.animate().scaleX(1.3f).scaleY(1.3f).setDuration(30);
        } else if (bookmark.isBookmarked() == Bookmark.BOOKMARK_TRUE) {
            setBookmark.setColorFilter(ContextCompat.getColor(this, R.color.selected));
            setBookmark.animate().scaleX(1.3f).scaleY(1.3f).setDuration(30);
        }
    }

    private void chooseMood(String mood) {
        if (moodToday.getMood().equals(mood)) {
            mood = Constants.MOOD_TODAY;
        }
        moodToday.setMood(mood);
        moodRef.setValue(moodToday);
        setMoodBlack();
        checkMood(moodToday);
    }

    private void checkMood(Mood moodToday) {
        if (moodToday.getMood().equals(Constants.MOOD_EMPTY)) {
            setMoodBlack();
            tvSetMood.setText(Constants.MOOD_TODAY);
        } else if (moodToday.getMood().equals(Constants.MOOD_HAPPY)) {
            tvSetMood.setText(Constants.MOOD_HAPPY);
            moodHappy.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (moodToday.getMood().equals(Constants.MOOD_SAD)) {
            tvSetMood.setText(Constants.MOOD_SAD);
            moodSad.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (moodToday.getMood().equals(Constants.MOOD_CHEEKY)) {
            tvSetMood.setText(Constants.MOOD_CHEEKY);
            moodHorny.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (moodToday.getMood().equals(Constants.MOOD_VERYHAPPY)) {
            tvSetMood.setText(Constants.MOOD_VERYHAPPY);
            moodVeryHappy.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (moodToday.getMood().equals(Constants.MOOD_SHITTY)) {
            tvSetMood.setText(Constants.MOOD_SHITTY);
            moodShitty.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (moodToday.getMood().equals(Constants.MOOD_COOL)) {
            tvSetMood.setText(Constants.MOOD_COOL);
            moodCool.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (moodToday.getMood().equals(Constants.MOOD_SILLY)) {
            tvSetMood.setText(Constants.MOOD_SILLY);
            moodSilly.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (moodToday.getMood().equals(Constants.MOOD_NEUTRAL)) {
            tvSetMood.setText(Constants.MOOD_NEUTRAL);
            moodNeutral.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        }
    }

    private void setMoodBlack() {
        moodHappy.setColorFilter(ContextCompat.getColor(this, R.color.black));
        moodVeryHappy.setColorFilter(ContextCompat.getColor(this, R.color.black));
        moodNeutral.setColorFilter(ContextCompat.getColor(this, R.color.black));
        moodCool.setColorFilter(ContextCompat.getColor(this, R.color.black));
        moodShitty.setColorFilter(ContextCompat.getColor(this, R.color.black));
        moodSad.setColorFilter(ContextCompat.getColor(this, R.color.black));
        moodHorny.setColorFilter(ContextCompat.getColor(this, R.color.black));
        moodSilly.setColorFilter(ContextCompat.getColor(this, R.color.black));
    }

    private void chooseWeather(String weather) {
        if (weatherToday.getWeather().equals(weather)){
            weather = Constants.WEATHER_EMPTY;
        }
        weatherToday.setWeather(weather);
        weatherRef.setValue(weatherToday);
        setWeatherBlack();
        checkWeather(weatherToday);
    }

    private void setWeatherBlack() {
        weatherCloudy.setColorFilter(ContextCompat.getColor(this, R.color.black));
        weatherSunny.setColorFilter(ContextCompat.getColor(this, R.color.black));
        weatherThunder.setColorFilter(ContextCompat.getColor(this, R.color.black));
        weatherFoggy.setColorFilter(ContextCompat.getColor(this, R.color.black));
        weatherSnowing.setColorFilter(ContextCompat.getColor(this, R.color.black));
        weatherRaining.setColorFilter(ContextCompat.getColor(this, R.color.black));
        weatherDrizzle.setColorFilter(ContextCompat.getColor(this, R.color.black));
        weatherWindy.setColorFilter(ContextCompat.getColor(this, R.color.black));
    }

    private void checkWeather(Weather weatherToday) {
        if (weatherToday.getWeather().equals(Constants.WEATHER_EMPTY)) {
            setMoodBlack();
            tv_weather.setText(Constants.WEATHER_EMPTY);
        } else if (weatherToday.getWeather().equals(Constants.WEATHER_SUNNY)) {
            tv_weather.setText(Constants.WEATHER_SUNNY);
            weatherSunny.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (weatherToday.getWeather().equals(Constants.WEATHER_CLOUDY)) {
            tv_weather.setText(Constants.WEATHER_CLOUDY);
            weatherCloudy.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (weatherToday.getWeather().equals(Constants.WEATHER_WINDY)) {
            tv_weather.setText(Constants.WEATHER_WINDY);
            weatherWindy.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (weatherToday.getWeather().equals(Constants.WEATHER_DRIZZLE)) {
            tv_weather.setText(Constants.WEATHER_DRIZZLE);
            weatherDrizzle.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (weatherToday.getWeather().equals(Constants.WEATHER_RAINING)) {
            tv_weather.setText(Constants.WEATHER_RAINING);
            weatherRaining.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (weatherToday.getWeather().equals(Constants.WEATHER_SNOW)) {
            tv_weather.setText(Constants.WEATHER_SNOW);
            weatherSnowing.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (weatherToday.getWeather().equals(Constants.WEATHER_FOG)) {
            tv_weather.setText(Constants.WEATHER_FOG);
            weatherFoggy.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        } else if (weatherToday.getWeather().equals(Constants.WEATHER_THUNDER)) {
            tv_weather.setText(Constants.WEATHER_THUNDER);
            weatherThunder.setColorFilter(ContextCompat.getColor(this, R.color.selected));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibtn_setBookmark:
                setBookmarked(!bookmark.isBookmarked());
                break;
            case R.id.ibtn_happy:
                chooseMood(Constants.MOOD_HAPPY);
                break;
            case R.id.ibtn_sad:
                chooseMood(Constants.MOOD_SAD);
                break;
            case R.id.ibtn_neutral:
                chooseMood(Constants.MOOD_NEUTRAL);
                break;
            case R.id.ibtn_cool:
                chooseMood(Constants.MOOD_COOL);
                break;
            case R.id.ibtn_veryHappy:
                chooseMood(Constants.MOOD_VERYHAPPY);
                break;
            case R.id.ibtn_shitty:
                chooseMood(Constants.MOOD_SHITTY);
                break;
            case R.id.ibtn_silly:
                chooseMood(Constants.MOOD_SILLY);
                break;
            case R.id.ibtn_horny:
                chooseMood(Constants.MOOD_CHEEKY);
                break;
            case R.id.ibtn_sunny:
                chooseWeather(Constants.WEATHER_SUNNY);
                break;
            case R.id.ibtn_windy:
                chooseWeather(Constants.WEATHER_WINDY);
                break;
            case R.id.ibtn_cloudy:
                chooseWeather(Constants.WEATHER_CLOUDY);
                break;
            case R.id.ibtn_raining:
                chooseWeather(Constants.WEATHER_RAINING);
                break;
            case R.id.ibtn_drizzle:
                chooseWeather(Constants.WEATHER_DRIZZLE);
                break;
            case R.id.ibtn_thunderstorm:
                chooseWeather(Constants.WEATHER_THUNDER);
                break;
            case R.id.ibtn_foggy:
                chooseWeather(Constants.WEATHER_FOG);
                break;
            case R.id.ibtn_snowing:
                chooseWeather(Constants.WEATHER_SNOW);
                break;
            case R.id.btn_done:
                finish();
                break;
        }
    }
}


