package com.example.matthewtaila.dateentries;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.matthewtaila.dateentries.constants.Constants;
import com.example.matthewtaila.dateentries.constants.FirebaseUtilConstants;
import com.example.matthewtaila.dateentries.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class LoginInActivity extends AppCompatActivity {

    // - UI
    EditText userName;
    Button logInButton;

    // - Login Variables
    String userLog;
    User user;

    // - FireBase
    DatabaseReference userFBRef;
    public static final String STUB_USER_ID = "-KO7wmSaHwPuFv89MivC";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_in);
        userFBRef = FirebaseUtilConstants.DATE_ENTRIES_REF.child(FirebaseUtilConstants.FB_USERS);
        referenceViews();
    }

    // - Reference Views from XML
    private void referenceViews() {
        userName = (EditText) findViewById(R.id.et_username);
        logInButton = (Button) findViewById(R.id.logIn);
    }

    public void logInMethod(View view) {
        final DatabaseReference addUserReference = FirebaseUtilConstants.DATE_ENTRIES_REF.child(FirebaseUtilConstants.FB_USERS).child(STUB_USER_ID);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    user = dataSnapshot.getValue(User.class);
                    startDateEntryActivity(user);
                } else if (!dataSnapshot.exists()) {
                    storeUser();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        addUserReference.addListenerForSingleValueEvent(valueEventListener);
    }

   public void startDateEntryActivity(User user){
        Intent intent = new Intent(this, DateEntryActivity.class);
        intent.putExtra(Constants.KEY_USER, user);
        startActivity(intent);
    }

    // - Stub login
    private void storeUser() {
        userLog = userName.getText().toString();
        user = new User();
        user.setUserName(userLog);
        DatabaseReference storeUserRef = FirebaseUtilConstants.DATE_ENTRIES_REF.child(FirebaseUtilConstants.FB_USERS);
        storeUserRef = storeUserRef.push();
        user.setKey(storeUserRef.getKey());
        storeUserRef.setValue(user);
        startDateEntryActivity(user);
    }

}

