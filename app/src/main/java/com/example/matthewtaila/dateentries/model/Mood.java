package com.example.matthewtaila.dateentries.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.matthewtaila.dateentries.constants.Constants;

/**
 * Created by matthewtaila on 8/18/16.
 */
public class Mood implements Parcelable {

    String mood = Constants.MOOD_TODAY;

    public Mood() {
    }

    protected Mood(Parcel in) {
        mood = in.readString();
    }

    public static final Creator<Mood> CREATOR = new Creator<Mood>() {
        @Override
        public Mood createFromParcel(Parcel in) {
            return new Mood(in);
        }

        @Override
        public Mood[] newArray(int size) {
            return new Mood[size];
        }
    };

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mood);
    }
}
